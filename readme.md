This is the result of the test task.

Task:
Creating layout from png file. Do not use pictures in the layout (except the logo). Some elements need to be active. Use js library for graphs .

---

Source image lies in "images/rees-215.png"

I used random data for graphs.

Push the button 'ВСЕ ЗАКАЗЫ', enter date and press enter - it will refresh orders table and refresh graph.

Respnsive layout (min ~ 700 pixels).

In this task I used:
Bootstrap, jQuery, moment.js, flot.js for charts.

---

Это Результат тестового задания.

Необходимо было сверстать страницу с некоторыми активными элементами и без использования картинок.

Картинка есть только в лого страницы.

Исходный макет лежит в "images/rees-215.png"

Графики и диаграммы строятся по генерируемым данным.

Можем нажать на ВСЕ ЗАКАЗЫ, ввести дату в поле и нажать ENTER - генерируются новые случайные данные, обновляется таблица с заказами - перестраивается график и выделяется указанная дата на графике.

Верстка адаптирована где-то до 700 px

Используется bootstrap, jquery, moment.js, для графиков flot.js
