(function() {

  $(function() {
    var switchOff, switchOn;
    switchOff = function(round) {
      round.removeClass('on');
      round.addClass('off');
      $('#with-swither').find('.grey-text').first().removeClass('active');
      $('#with-swither').find('.grey-text').last().addClass('active');
      return $('#a-b-test .dropdown-toggle').text('Выключен');
    };
    switchOn = function(round) {
      round.removeClass('off');
      round.addClass('on');
      $('#with-swither').find('.grey-text').last().removeClass('active');
      $('#with-swither').find('.grey-text').first().addClass('active');
      return $('#a-b-test .dropdown-toggle').text('Включен');
    };
    $('#back').on('click', function() {
      var round;
      round = $(this).find('#round');
      if (round.hasClass('on')) {
        return switchOff(round);
      } else {
        return switchOn(round);
      }
    });
    $('#switch-on').on('click', function() {
      return switchOn($('#round'));
    });
    $('#switch-off').on('click', function() {
      return switchOff($('#round'));
    });
    $("#date-mask").inputmask().on('keyup', function(e) {
      var code, parse_date, _i, _results;
      code = e.keyCode || e.which;
      if (code === 13) {
        parse_date = moment($(this).val(), 'DD/MM/YYYY');
        if (parse_date <= moment() && parse_date >= moment('01 jan 2000')) {
          Rees.buildDiagram('day', parse_date);
          $('#orders .uppercase').text(parse_date.lang("ru").format('DD MMMM'));
          $('#orders .dropdown').removeClass('fix-open');
          $('.right-col tbody').html('');
          _results = [];
          for (_i = 0; _i <= 19; _i++) {
            _results.push($('.right-col tbody').append('<tr><td>' + Rees.random(10000, 90000) + '</td><td>' + Rees.random(10, 24).toString() + ':' + Rees.random(10, 59).toString() + '</td><td>' + Rees.random(10000, 90000) + '</td><td>' + Rees.random(10000, 90000) + ' руб.</td><td>' + Rees.random(90000, 190000) + ' руб.</td></tr><tr></tr>'));
          }
          return _results;
        } else {
          return alert;
        }
      }
    });
    $('#orders .uppercase').text(moment(new Date()).lang("ru").format('DD MMMM'));
    $('#orders .dropdown').on('click', function() {
      return $(this).addClass('fix-open');
    });
    for (var n = 0; n <= 18; n++) {
      $('.right-col tbody').append('<tr><td>098089889</td><td>13:48</td><td>829342738</td><td>864.56 руб.</td><td>9765.38 руб.</td></tr><tr></tr>');
    }


    if ($('.left-col').height() > $('.right-col').height()) {
      $('.right-col  .blue-block').css('height', $('.left-col').height() + 'px');
    }

    return $('body').on('click', function(e) {
      if (!$(e.target).hasClass('.dropdown-menu') && $(e.target).closest('.dropdown-menu').length === 0) {
        return $('.dropdown').removeClass('fix-open');
      }
    });



  });

}).call(this);
